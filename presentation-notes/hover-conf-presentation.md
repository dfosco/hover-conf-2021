## **Hover Conf 2021**
### What’s New in CSS
---

## What’s New In CSS
### **Adam Argyle, Developer Advocate @ Google**

![fit 35% right](images/adam.png)

--- 

### **Rapid-fire of 31 new CSS features**

---

![inline](images/toc.png)

---

#### https://**2021-hover-conf-new-in-css**.netlify.app

---

### Conditional Values

---

```css
div {
    flex-direction: if(100vw > 500px, row, column)
}
```

^ Simpler media query with an inline conditional! It's pretty much a JS ternary in CSS

---

```css
--size: small;
font-size: if(var(--size) = small, 2em)
           if(var(--size) = medium, 3em)
           if(var(--size) = large, 5em);
```

---

### Switch

---

```
.foo {
    background: switch(
        (width > 200px) red;
        (width > 400px) blue;
        default white;
    )
}
```
---

### Houdini Layout

---

```css
.houdini-masonry {
  display: layout(masonry);
}

.houdini-packery {
  display: layout(packery);
}
```

^ Javascript is always on the main thread of the browser rendering. Houdini allows you to create your own layout logic and functions to run directly on the browser engine, freeing space on the main JS thread

---

### Houdini Paint

---

```css
.powered-gradient {
  --powdered-gradient-direction: to-top;
  --powdered-gradient-color: white;
  --powdered-gradient-size: 1;

  background: paint(powdered-gradient), #111;
}
```

---

![fit 70%](images/houdini-paint.gif)

---

![fit 70%](images/houdini-curved-lines.gif)

---

### Scope

---

```css

.card {...}

.card__header {
  color: var(--text);
}

```

---

```jsx
import { css, cx } from '@emotion/css'

const color = 'white'

render(
  <div
    className={css`
      padding: 32px;
      background-color: hotpink;
      font-size: 24px;
      border-radius: 4px;
      &:hover {
        color: ${color};
      }
    `}
  >
    Hover to change color.
  </div>
)
```

---

```css
@scope (.card) {
  header {
    color: var(--text);
  }
}
```

---

```css
 @scope (.light-theme) {
  a { color: purple; }
 }

 @scope (.dark-theme) {
  a { color: plum; }
 }
```

---

### Container Queries

---

![inline autoplay loop](images/container-queries.mp4)

---

```css
.container-to-be-queried {
  contain: layout inline-size;
}
```

^ You apply `contain` to define this element as a parent container and which properties it should respond to

---

```css
@container (inline-size > 240px) {
  .item {
    flex-direction: row;
  }
}
```

^ And then declare how the child elements should react given the container inline-size 

---

### Nesting

---

```css
article {
  color: darkgray;

  & > a {
    color: var(--link-color);
  }
}
```

---

### Cascade Layers

---

```css
/* reset.css */

* { box-sizing: border-box; }
body { margin: 0; }
```
---

```css
layer reset {
  * { box-sizing: border-box; }
  body { margin: 0; }
}

```

---

```css
@layer reset { /* add more later */ }
```

---

```css
@import url(headings.css) layer(default);
@import url(links.css) layer(default);
```

---

```css
@layer default;
@layer theme;
@layer components;

@import url(theme.css) layer(theme);
```

---

```css
@layer default, theme, components;
```

---

### Foldables

---

![](images/surface-duo.jpeg)

---

![](images/foldables.png)

---

```css
/*
 env(fold-width)
 env(fold-left)
 env(fold-right)
*/
```

---

```css
main {
  display: grid;
  gap: env(fold-width);
  grid-template-columns: env(fold-left) 1fr;
}
```

---

```css
@media (spanning: single-fold-vertical) {
  aside {
    flex: 1 1 env(fold-left);
  }
}
```

---

###  Color Level 4

---

![](images/apple-4k.jpg)

---

![](images/drab.png)

---

![inline](images/lch-gradient.png)

---

![inline](images/color-5.png)

---

```css
#lab-and-lch {
  --ux-gray: lch(50% 0 0);
  --rad-pink: lch(50% 200 230);
  --rad-pink: lab(150% 160 0);
  --pale-purple: lab(75% 50 -50);
}
```

---

### Color Level 5

---

```css
.color-mix {
  --pink: color-mix(red, white);

  --brand: #0af;
  --text1: color-mix(var(--brand) 25%, black);
  --text2: color-mix(var(--brand) 40%, black);
}
```

---

```css
.color-contrast {
  color: color-contrast(
    var(--bg)
    vs
    black, white
  );
}
```

---

![inline autoplay loop](images/color-level-5-video.mp4)

---

### Data Saver

```css
@media (prefers-reduced-data: reduce) {
  header {
    background-image: url(/grunge.avif);
  }
}
```

---

```css
@media (prefers-reduced-data: no-preference) {
  @font-face {
    font-family: 'Radness';
    src: url(megafile.woff2);
  }
}
```