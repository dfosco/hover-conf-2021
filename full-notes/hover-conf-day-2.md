
### Neurodiversity (and why you hate CSS)
#### Facundo Corradini, Front-end Developer Independent

- Neurodivergence: people have multiple ways of thinking
- There are multiple types of intelligence

- Designers and people that work visually usually are stronger in visual-spatial intelligence
- Developers that work with logic pgrogramming exercise more logical-mathematical intelligence

- Is CSS a programing language? Yes, but I believe CSS is more of a language, than it is programming

- CSS _describes_ the interface

- CSS is really, really contextual: containing blocks, formatting context, inheritance, origin, specificity, etc...

---

- The first thing that I tweeted when I found out that I was speaking here was: "I'm over the moon about it." And I'm sure no one thought that went any further than Neil Armstrong.

- CSS also has idioms:

```css
.clearfix::after {
    ...
}
```

By reading this, everyone knows all I want is to fix the clearfix hack, not necessarily use the properties within

---

For the most part when it comes to the layout and styles of our websites and apps, I believe CSS relies primarily on verbal-linguistic intelligence.

And that's why both designers and developers can struggle with it.

CSS: Verbal-linguistic intelligence

---

#### How to best learn CSS? Approach it like a second spoken language.

- Relying on automation: CSS frameworks generate standardization and can impair the true learning of the language
- Crash courses: Learning the basics really fast helps to pick some things, but once you go out of your depth, CSS can crash you really hard (specificity, debugging, box model)

- Learning the foundations: box-model, writing-modes. Read the specs for new modules!

#### A language only stays with you if you use it

#### Hire a translator

Let people shine in their trade! Writers of CSS can have a huge impact on your organization

#### Include diverse people

---

### The New CSS Logical Properties
#### Elad Shechter, CSS/HTML Architect Independent

#### The internet wasn't created for regular websites like you are using today.

Think about one of the basics things that you are using in CSS these days, like the models of CSS, of CSS flexbox. CSS flexbox only started to being implemented in web browsers in 2012!
 
English is written left-to-right, and semitic languages are right-to-left. To avoid duplicating stylesheets for multi-language websites, we now have **CSS writing modes**

#### Axis

Every site has two axis:

- Block Axis: the main axis of the website, usually top to bottom
- Inline axis: By default, going left-to-right, the direction of the text

In traditional japanese, the text is written top to bottom, right to left. But how to deal with headers, footers? Do we invert everything? Here's how we deal with it.

#### Writing Mode

- Block axis - Defines the flow direction of the website

`writing-mode: horizontal-tb` -- vertical scroll
`writing-mode: vertical-tb` -- horizontal scroll, text written in vertical direction
`writing-mode: vertical-lr` -- horizontal scroll, text written in vertical direction, starting on the left side, 

- Inline axis - Defines the text direction of the website

#### Old Physical Properties

Based on physical attributes of the physical world

Top / Right / Bottom / Left

#### New Logical Properties

Logical attributes, **based on the site axis**, in english:

```
            block-start 
inline-start            inline-end
             block-end
```

block axis (website flow)
inline axis (content flow)

But in traditional japanese, **everything is rotated 90°**!

```
            inline-start 
block-end            block-start
             inline-end
```

---

#### Update your thinking

##### In english defaults
- Top and bottom are the block axis
- Left and right are the inline axis

- Width is now inline-size
- Height: is now block-size

##### Positions

```css
position: fixed;
top: 0;
left: 0
```

Becomes:

```css
position: fixed;
inset-block-start: 0;
inset-inline-start: 0;
```

You can also do:

```css
position: fixed;
inset: 0 0 0 0; 
/* block-start inline-end block-end inline-start */
``` 

#### More Updates

```css
text-align: left /* text-align: start */
```

```css
resize: horizontal /* resize: inline */
resize: vertical /* resize: block */
```

#### Flexbox and Grid

Flex has always been "logical" -- `flex-start`, `flex-end`!

Grid is the same: `grid-column-start`, `grid-column-end`, etc... no need to change values!

#### Updating your websites

It can be tricky... you're changing your CSS, and _you won't see any changes_! 

To test your changes, just add to your HTML:

- `direction: rtl` for hebrew
- `writing-mode: vertical-rl` for traditional japanese
- `writing-mode: vertical-lr` for traditional mongolian

We are finally able to create a single website with a single stylesheet for every language in the world!

#### Shorthand limitations

- Shorthand like `margin: 10 20 20 10` still has no equivalent logical shorthand. 
- The current proposal to approach this is `margin: logical 10 20 20 10`

- However, I don't like the idea of adding `logical` to every single shorthand to your site -- it's repetitive. So I thought of a new property:

```css

html {
    flow-mode: logical; /* or */
    flow-mode: physical;
}
```

#### Responsive design on a logical website

```css
@media (max-inline-size: 1000px) {
    /* Does not work... yet */
}
```

#### Browser Support

 - All box model properties `margin/padding/border`
