# Hover Conf Day 1

### What’s New In CSS 2021
#### Adam Argyle, Developer Advocate Google

[See all slides](https://2021-hover-conf-new-in-css.netlify.app/#toc)

Some of these features are available today through PostCSS or plugins, but others are still in the horizon.

**Rapid-rire of 31 new CSS features**

Starting from high-risk features, going down to stable and (potentially) available features

#### High Risk

These might never make it into CSS!

#### Conditional Values

```css
div {
  flex-flow: if(100vw > 50px, row, column);
}
```

```css
background:
  if(
      var(--raised) = on, linear-gradient(white, transparent),
      red
  )
```

#### Switch

.foo {
    background: switch(
        (width > 200px) red;
        (width > 400px) blue;
        default white;
    )
}


#### Relative Units

```css
.more-like-em {
    border-radius: 2lh; /* line height */
    border-width: 2cap; /* cap height */
}

.logical-viewport-units {
    inset: 10vi 25vb; /* Logical equivalent to viewport units */
}

.more-like-rem {
    margin-block: 2rlh /* Root line height */
}
```

#### Houdini

Enable advanced styling processes for layout, paint, animation within the browser engine, so it saves performance when the browser is actually processing the page later on

.houdini-masonry {
  display: layout(masonry); /* (masonry) is a custom Houdini style function that does not need JS to calculate layout */
}

.houdini-packery {
  display: layout(packery); /* (packery) is a Houdini function! */
}

#### Scope

```css
.card {...}

.card__header {
    color: var(--text);
}
```

```css
@scope (.card) {
    header {
        color: var(--text);
    }
}
```

```css
@scope (.media-block) to (.content) {
    header { ... }
    /* The styles in this scope only apply between these two elements on the DOM */
}
```

#### Moderate Risk


#### Container Queries

```css
.container-to-be-queried {
    contain: layout inline-size;
}
```

When contain is applied, the browser knows this will not be applied to the rest of the page, only to children of this specific container.

```css
@container (inline-size > 240px) {
    .item {
        flex-direction: row
    }
}
```

```css
section {
    @container (inline-size <= 60ch) {
        & {
            padding-inline: 1ch
        }
    }
}
```

#### Leading Trim

Allows to chop off the excess of text boxes on elements! From the cap height to the baseline grid.


```css
 p {
     leading-trim: both
 }
```

```css
 p {
     text-edge: cap alphabetic;
     leading-trim: [SEE FROM SCREENSHOT]
 }
```

#### Houdini Paint

Brings in a JS file worklet (running off the browser main thread) to paint backgrounds and elements using a custom `paint()` function with multiple arguments and values!

[FILL IN FROM SG]

Browser support for Paint API is pretty good, getting better

#### Scroll Timeline

Remedies JS solutions to track element styling _exactly_ to the browser's native scroll position. Works great for animations!

```css
@supports (animation-timeline: works) {
    ...
}
```

```css
@scroll-timeline scroll-fade {
  time-range: 4s;
}

@scroll-timeline scroll-fade {
  time-range: 4s;
}

nav {
  opacity: 0;
  animation: fade-in 1s linear both;
  animation-timeline: scroll-fade; 
}
```
Animation timeline is perfectly linked to the scroll position of the window! They act like overlapping timelines on a video editor!


#### Spelling and Grammar

```html
<p
  spellcheck
  contenteditable
>
<!-- Will enable browser spellcheck on the text being edited; Works today -->
</p>
```

This is new, though! Custom style 
```css
::spelling-error {
    text-decoration: wavy underline red;
}
```

#### Target Within

The :target-within pseudo-class applies to elements for which the :target pseudo class applies as well as to an element whose descendant in the flat tree (including non-element nodes, such as text nodes) matches the conditions for matching :target-within.

```css
article:target-within {
  border-block-start: var(--brand);
}
```

#### Nesting

YES! Native nesting!!!!!

Simpler than what Sass or PostCSS offers, but does the job, and in native CSS.
```css
article {
  color: darkgray;

  & > a {
    color: var(--link-color);
  }
}
```
```css
code > pre {
  @media (hover) {
    &:hover { /* & turns into the code > pre context */
      color: hotpink;
    }
  }
}
```

```css
code > pre {
  @media (hover) {
    @nest &:hover { 
    /*@nest  allows you to target your parent anywhere in your selector  */
      color: hotpink;
    }
  }
}

article {
  @nest section:focus-within > & {
      /* Same here, & is the 'article' context, but as part of this new selector */
    color: hotpink;
  }
}
```

#### Cascade Layers

```
So this is also known as @layer, and what's really important about this and the problem space is - you have so many different styles coming in from your page, and sometimes they're at the top.

Sometimes they're at the bottom.

Sometimes you own the style.

Sometimes the style is coming from somewhere else.

Sometimes the style came from npm.

Sometimes you bundled.

Sometimes you didn't bundle.

There's just so many origins.

So many types - first party, third party, all these styles are coming in and they essentially append or they prepend or JavaScript loaded more styles and stuck them here , and they stuck them there and took them over here.

There's all these places to stick styles and no great way to orchestrate all of it, or at least to articulate areas for these things that are coming in asynchronously to go into.

So that's essentially how I see this, as the problem space: this is a place for me to name various layers of my CSS style stack.
```

Layer lets you import and order styles into different pieces, with your source order OVERRIDING specificity.

```css

@layer reset {
  * { box-sizing: border-box; }
  body { margin: 0; }
}

@layer reset { /* add more later */ }

```

```css
@import url(headings.css) layer(default); /* Import this file into this layer */
@import url(links.css) layer(default); /* Import this other file into this other layer */
```

```css
@layer default;
@layer theme;
@layer components;
```

```css
@import url(theme.css) layer(theme);
```

```css
@layer framework.theme {
  p {
    color: rebeccapurple;
  }
}
```

#### Foldables

A bunch of CSS environment variables (🤯🤯🤯) and @media attributes for foldable screen devices

```css
env(fold-width)
env(fold-left)
@media (spanning: single-fold-vertical)
```

#### Color Level 5

Enables mixing colors and deeper color spaces!

```css
.color-mix {
  --pink: color-mix(red, white);

  --brand: #0af;
  --text1: color-mix(var(--brand) 25%, black);
  --text2: color-mix(var(--brand) 40%, black);
}
```

```css
.color-contrast {
  color: color-contrast( var(--bg) vs black, white);
}
```

```css
--text-on-bg: color-contrast(
  var(--bg-blue-1)
  vs
  var(--text-subdued),
  var(--text-light),
  var(--text-lightest)
) 
```

Hey CSS, here are all these colors, pick the one with higher contrast

```css
.color-adjust {
  --brand: #0af;
  --darker: color-adjust(var(--brand) lightness -50%);
  --lighter: color-adjust(var(--brand) lightness +50%);
}
```

Sass-like color adjustment functions. Neat!

```css
.relative-color {
  --color: #0af;
  --abs-change: lch(from var(--color) 75% c h);
  --rel-change: lch(from var(--color) l calc(c-20%) h);
}
```

Destructuring a single color value into HSL channels, and let you apply operations to specific channels within a single color value!


#### Masonry

Enable a lot of the masonry effects (aka Pinterest) commonly built with JS today

```css
.masonry {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: masonry;
}

.grid {
  display: inline-grid;
  direction: rtl;
  column-gap: 1ch;
  grid: repeat(4, 2em) / masonry;
}
```

#### Media Queries 5

```css
@media (width <= 320px) { /* Simpler to write! */
  body {
    padding-block: var(--sm-space);
  }
}
```

```css
@custom-media --motionOK (prefers-reduced-motion: no-preference);
/* Lets you stash a media query within a CSS variable! */
@media (--motionOK) {
  .card {
    transition: transform .2s ease;
  }
}
```

```css
.card {
  @media (--motionOK) { & {
    transition: transform .2s ease;
  }}
} /*  */
```

```css
@media (1024px >= width >= 320px) { /* This reads SO WELL! Very intuitive */
  body {
    padding-block: 1rem;
  }
}
```

#### Low Risk

Try it out, but maybe with @supports and fallbacks 🙂

#### Color Level 4

Introduces **LCH** that expands the color space! It gives access to a larger range of colors that newer screens can display. Today's screens can display much more colors than CSS actually supports 💥💥💥

```css
    #hex-with-alpha {
    color: #0f08;
    color: #00ff0088; /*Longer hex value that includes alpha values in it*/
    }
```

```css
#functional-notation { /* Simpler syntax with no commas, no more hsla() and rgba() with "/ 80%" */
  color: hsl(0deg 0% 0%);
  color: hsl(2rad 50% 50% / 80%);
  color: rgb(0 0 0);
  color: rgb(255 255 255 / .25);
}
```

```css
/* LCH and Lab map more closely to the way humans perceive light and color */
/* LCH = Lightness, Chroma, Hue */

#lab-and-lch {
  --ux-gray: lch(50% 0 0); /* What scientists think it's 50% lightness (gray) for our eye */
  --rad-pink: lch(50% 200 230);
  --rad-pink: lab(150% 160 0);
  --pale-purple: lab(75% 50 -50);
}
```

```css
/* Lets */
#color-function {
  --rad-pink: color(display-p3 1 0 1);
  --rad-pink: color(lab 50% 150 -50);
  --rad-pink: color(srgb 100% 0% 50%);
}
```

#### HD Color

```css
/* Lets you target HDR displays (without identifying the display type) */
/* Can display HRD colors? Yes = Use this color */
/* Safari stable has this already! */
@media (dynamic-range: high) {
  .neon-pink {
    --neon-glow: color(display-p3 1 0 1);
  }
}
```

#### Typed Custom Properties

Typescript for your CSS! No silently breaking properties because they were passed invalid values! Typestyle!

```css
@property --hue {
  initial-value: 0;
  inherits: false;
  syntax: '<number>';
}
```

```css
@property --milliseconds {
  syntax: '<integer>';
  initial-value: 0;
  inherits: false;
}

.counter {
  counter-reset: ms var(--milliseconds);
  animation: count 1s steps(100) infinite;
}

@keyframes count { to {
  --milliseconds: 100;
}}
```

#### Content Visibility

Still has accessibility implications being worked out!

The content-visibility property controls whether or not an element renders its contents at all, along with forcing a strong set of containments, allowing user agents to potentially omit large swathes of layout and rendering work until it becomes needed. It has the following values:

```css
section {
  content-visibility: auto;
  contain-intrinsic-size: 1000px; /* Here's the size I need to be rendered */
}
```

#### Aspect Ratio

```css

.box {
  width: 200px;
  aspect-ratio: 1;
}

img.square {
  aspect-ratio: 1; /* No matter the height, your square will always be a square! */
  width: 100%;
  object-fit: contain;
}

.transition-it {
  aspect-ratio: 1/1;
  transition: aspect-ratio .5s ease;

  @media (orientation: landscape) { & {
    aspect-ratio: 16/9;
  }}
}
```

#### ::marker

Full styling customization to list style bullets

```css
li::marker {
  content: counter(list-item) "› ";
  color: hotpink;
}
```

Want to replace your markers with SVG? Sure!

```css
li::marker {
  content: url(/heart.svg);
  content: url(#heart);
  content: url("data:image/svg+xml;...");
}
```

#### Conic Gradients

```css
.conic-1 {
  background: conic-gradient(
    deeppink,
    rebeccapurple
  );
}

.conic-2 {
  background: conic-gradient( 
    /*Pass a third color to make a beautiful bottom-shadow effect on your gradient! */
    deeppink,
    rebeccapurple,
    deeppink
  );
}

.conic-3 {
  background: conic-gradient(
    at bottom left, /* Tuck it away in a corner to make it more subtle! */
    deeppink,
    cyan
  );
}
```

#### Containment

The technical base for container queries!

```css

article { /* Locks the content within the element */
  contain: content;
}

article {
  contain: layout; /* Locks the sizing of the element */
  contain: size;
  contain: paint;
}

```

#### ::focus-visible

Now enable mouse interactions to NOT have focus ring, only keyboard interactions

#### ::focus-within

Styling a parent element to change when its child is interacted with (focused)

#### Logical Properties

“I want to shake your right hand” >>> “I want to shake your dominant hand”

Let users define their default direction and writing-modes, and have properties respond to this user setting!

```css
p {
  max-inline-size: 40ch; /* Can be left-right or top-bottom depending on the users' text direction */
}

small {
  padding-inline: 1ch; /*Same as padding-left/padding-right in English, */
  text-align: end; /* Aligns to the end of the text line, no matter where that is*/
}

article {
  margin-block: 1ch;
}

button {
  border-inline: 1px solid currentColor; /* Styles start/end borders (left/right) */
}
```

#### :is( ) and :where( )

:is( ) and :where( ) let you work around specificity

[NE: Add more info]


```css
/* Styles the first-letter only for elements matching these selectors */
:is(a,h1,h2,h3)::first-letter { 
  text-transform: uppercase;
}

:where(#target, p) { 
    /* Lets you zero-out specificity of these selectors -- "where these, do this instead" */
  color: red;
}
```

#### @media (prefers-reduced-data)

@media (prefers-reduced-data: reduce) { ... }
@media (prefers-reduced-data: no-preference) { ... }

```css
@media (prefers-reduced-data: reduce) {
  /* If the user prefers reduced data, download a smaller image */
  header {
    background-image: url(/grunge.avif);
  }
}
```

```css
@media (prefers-reduced-data: no-preference) {
  /* If the user does not care, download large file */
  @font-face {
    font-family: 'Radness';
    src: url(megafile.woff2);
  }
}
```

#### ::cue( )

Dedicated pseudo-selector to style the subtitles within the HTML <video> element. The `cue` element exists today, but there's currently no way to style it.

```css
::cue {
    color: white;
    background-color: hsl(0 0% 0% / 90%);
}

::cue(b) { /* You can even subtitle specific elements in your subtitles, like bold! */
  background: var(--brand);
  color: var(--text-on-brand);
  font-weight: var(--brand-bold);
}
```

---

### Container Queries & The Future of CSS
#### Miriam Suzanne, Co-founder Oddbird

[See slides here](https://slides.oddbird.net/css-next/hover/)

In order to talk about the future of CSS, we need to understand where it is today.

CSS exists to make styles responsive, not only to viewport sizes, but also to user settings.

> “Web for all. Web for Everything” 
> W3C mission statement

#### Order of overwriting styles on the browser

- Author
- User Preferences
- User Agent (Browser)

#### The cascade resolves merge conflicts

It uses specificity to define the winner in overlapping styles.

Out of all selector types, classes and attributes are really the only reusable units of styling 

#### Cascade Layers

```css
@layer default { /* lowest */ }
@layer theme { /* higher */ }
@layer components { /* highest */ }
```

Layers override each other uniquely based in order, and traditional specificity only matters within the layers

Can even declare layers just by names to set their order, and later on populate their styles:

```css
@layer default, theme, components;

@layer default {
 background: red;
}
```

We can create nested or namespaced layers using a dot notation to combine the names, or we can actually nest those layer rules.

Layers gives us more cascade control, and reduces the need for hacks.

#### Scoped Styles

Build tools and naming conventions like BEM provide (or try to) scoped styles today.

We should be able to style a tab component or a media embed without fearing we might style everything else.

Scopes are defined in CSS and can be reused across components or overlap and cascade together.


We're proposing an `@scope` rule, which accepts both a scope route selector (in this case, media), and a lower boundary selector (in this case, content).

```css
@scope (.media) to (.content) {
 /* Only applies styles that are between .media AND .content in DOM order */
  { ... }
}

@scope ([data-theme=light]) {
  a { color: purple; }
}
@scope ([data-theme=dark]) {
  a { color: plum; }
}
```

#### Container Queries

Long awaited CSS feature! First, establish container context: 

```css
.container {
  contain: size layout;
}

.sidebar, main, .grid-item {
  /* contain: size layout; */
  contain: inline-size layout;
}
```

Then, declare the conditions that apply within that context:

```css
@container (min-width: 40em) {
  .card { border: none }
  h2 { text-size: 1.6em }
}
```

---

### The State of CSS-in-JS
#### Mark Dalgleish, Front End Lead SEEK

- CSS-in-JS has been a very active space
- Styled-Components and Emotion are the current winners
- SC was the first to go mainstream (2.6mil weekly downloads)
- Emotion is very much used by component libraries (helped push it to 3.9mil weekly downloads)

- With a few lines of JS we created an adaptive component that changes style based on interaction

- Emotion: made a breakthrough where you can write “inline” styles with native CSS syntax that get compiled into CSS classes on the DOM

#### Is CSS-in-JS a solved problem?

- “Not invented here”: People building libraries or in large companies don't want to bet on uncertain solutions, so that generates even more libraries.

- The community is still constantly evolving

#### Goober

- Focuses on bundle-size and performance
- < 1 kb
- +200k operations/second

#### Stitches

- Allow you to have high-level abstraction with props

[stitches]

- Also makes CSS Variables first-level concepts

#### vanilla-extract

- Enables extraction of CSS, zero-runtime css in typescript (??)
- On build-time the .ts files are analyzed, static CSS is extracted from it, and none of your vanilla-extract .ts goes into your final bundle

- Also lets you declare Style Variables in .ts that are exported to CSS Variables

[vanilla-extract--css-variables]

- It's a minimal abstraction over standard CSS

#### Compiled

- Also does static analysis over your JS and extracts it as static CSS with no logic on runtime

- If your JS is too complex and static analysis fails, it processes your styles on runtime

#### So... what should you pick?

- Nobody will get fired for picking SC or Emotion, these are very stable projects 
(Still might not fit the tech stack or engineering processes of every company... so your mileage might vary)
- The 

---

### CSS Aspect Ratio
#### Anton Ball, Front End Developer Doist

- Aspect Ratio: ratio between the width and height of an element; For every unit of width, there will be a different unit of height

- Common aspect ratios:
  * 1:1
  * 3:4
  * 16:9 

- There are JS libraries and even CSS hacks to work with aspect ration in browsers, but no native solutions

- Video embeds and grid elements are two common use cases for aspect ratio.

- If we need an element to maintain an aspect ratio today, some options are:
  * Padding top hack
  * Fitvids.js -- for video embeds

- New aspect-ratio property!

```css
  aspect-ratio: auto|<ratio>
```

```css
  aspect-ratio: 16/9
```

- Intrinsic Aspect Ratio
  * iframe
  * video
  * embed
  * img

- Preferred Aspect Ratio: The aspect-ratio CSS property sets a **preferred** aspect ratio.

> “Aspect-ratio as the weakest way to size an element”

- Aspect ratio is ignored if you're setting height and width for an element
- Overflowing content also overrides aspect-ratio when the container resizes to keep the content visible

- CSS Aspect Ratio will solve huge issues for developers when displaying image or video content in the web

---

### CSS Variables for Real Life
#### Matt Colman, Senior Software Engineer Atlassian

- CSS Variables initial thoughts
  * Interesting, looks fun
  * currentColor is a CSS Var, right?
  * Dark Mode!!!1

- Real Life Examples
  * Managing colors
  * Reducing duplicate code

- CSS Variables enable you to have styles for different states with minimal code and no JS:

- Using CSS Variables for fallbacks between 
  * selected-color
  * hover-color
  * background-color
  
- Browsers can display the variable names (e.g.: h700, brand-color-primary) that are shared by your design system and Figma libraries, instead of just the compiled values, making debugging much easier.

---

### Understanding display
#### Rachel Andrew, Freelance writer of words and code Independent

[See slides](https://noti.st/rachelandrew/VeraEX/understanding-display)

- CSS1 Recommendation - December 1996 

- "Formatting model" later became the Box Model

- Some things are block things; Other things are inline things

- You write plain HTML, and content is readable. You don't have to position every single line of text.

- Being able to change the value of display between block and inline is important

- Here is a block thing; Here is an inline thing; This is the box model. It's Very Important and kind of weird.

#### Display is the key

`display` puts the rest of the layout in context.

- The block direction is the direction that paragraphs lay out in your writing mode

- The inline direction is the direction in which sentences run in your writing mode

- A well structured HTML document means you are working in the browser rather than against it.

- CSS display really works as boxes within boxes. 

> For each element, CSS generates zero or more boxes as specified by that element's display property

- display: block creates a block level box
- display: inline creates a inline level box
- display: flex creates a block level box with flex children

#### Creating a Flex Formatting Context

A formatting context describes how a thing behave. 

Paragraphs in a page participate in a block formatting context.

Lines in a paragraph are in an inline formatting context.

Flex items belong to a flex formatting context.

#### The value of display

- Changes how the box behaves among other boxes
- Defines how the children of that box behave

#### Refactored display specification

```css
display: block flex
/* Block level box with flex children*/
```

```css
display: block grid
/* Block level box with grid children*/
```

#### display: block flow-root

Using `overflow: auto` to fix a floated image that is leaking out of its block creates a new block formatting context -- and this makes a parent contain all its children. It reinserts the image into the block flow.

`display: block flow-root` reinstates the block formatting context explicitly for its children. That's all it does! It's `display: block (also clearfix hack please)`

#### display: flow-root

Resets collapsed margins to be contained within the block formatting context

#### Anonymous Boxes

Fragments of text such as 'one' and 'three' below are anonymous boxes, that act as wrapped elements -- if you make them `flex` children, they can be manipulated just as the `<span>` in the center:

```html
<p>One <span>two</span> three</p>
```
```css
p {
  display: flex;
  justify-content: space-between
  /* `one` and `three` will also be styled by this*/
}
```

#### Floating and Positioning

Taking items out of flow. In `position: absolute`, items are taken out of the page flow.

`float: left|right` items are also taken out of flow -- but text boxes surrounding still respect its space

But when you use `grid` or `flexbox`, what happens to floating?

When using `grid`, `float` is overwritten and ignored.

When using `position: absolute` with a `grid` child, the containing grid area becomes the relative parent, not the actual parent grid element.

`display: none` removes the boxes from flow, even anonymous boxes within. 

`display: contents` removes the box like none, but only the box and not its children. The children remain and become direct descendents of its... grandparent? 😅

#### Principal Box and List Items

`Principal Box` the name of the box around your list item. The list bullet is called a `Marker box`.

The new `::marker` pseudoelement is being introduced to allow direct styling the marker box, finally!

`display: list-item` turns anything into a list item!

```css
h1 {
  display: list-item
}

h1::marker {
  content: '🥕'
}
```

#### What's going on with Subgrid?

Subgrid: Allowing track definitions to be inherited by a grid on a child.

To use subgrid, first create a grid formatting context with `display: grid`. Then opt in columns or rows with the subgrid value.

The subgrid is not inheriting any values from the parent grid, rather than following its tracks